module.exports = {
    name: "greeter",

    events:{
        "greetingOnLogin"(payload) {
            console.log("Hello", payload.name);
        },
    },

    actions: {
        hello() {
            return 'Hello World';
        },
        howdy:{
            params: {
                name: "string ",
            },
            handler(ctx){
                return `Howdy ${ctx.params.name}?`;
            }
        }
    },

    methods: {
        generateGreetingMessage() {
            return 'Randomly generated greeting message';
        }
    },

    created() {
        console.log('Fired when the service instance created.');
    },
    
    async started() {
        console.log('Fired when `broker.start()` called.');
    },
    
    async stopped() {
        console.log('Fired when `broker.stop()` called.');
    }

};