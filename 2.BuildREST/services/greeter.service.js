module.exports = {
    name: "greeter",

    events:{
        "greetingOnLogin"(payload) {
            console.log("Hello", payload.name);
        },
    },

    hooks:{
        before:{
            hello(){
                console.log('Hello before hook');
            }
        },
        after:{
            '*'(ctx, res){
                console.log('All action after hook');
                res +=` from ${this.broker.nodeID}`;
                return res;
            }
        }
    },

    actions: {
        hello:{
            rest: {
				method: "GET",
				path: "/hello"
			},
            handler () {
                return 'Hello World';
            }   
        },
        howdy:{
            rest: "/howdy",
            params: {
                name: "string ",
            },
            handler(ctx){
                return `Howdy ${ctx.params.name}?`;
            }
        }
    },

    methods: {
        generateGreetingMessage() {
            return 'Randomly generated greeting message';
        }
    },

    created() {
        console.log('Fired when the service instance created.');
    },
    
    async started() {
        console.log('Fired when `broker.start()` called.');
    },
    
    async stopped() {
        console.log('Fired when `broker.stop()` called.');
    }

};