const DbMixin = require("../mixins/db.mixin");

module.exports = {
	name: "greeting",
	mixins: [DbMixin("greetings")],

	/**
	 * Settings
	 */
	settings: {
		// Available fields in the responses
		fields: ["_id", "greeting",],

		// Validator for the `create` & `insert` actions.
		entityValidator: {
			greeting: "string|min:4",
			at: "string|min:3"
		}
	},


	actions: {
		/**
		 * The "moleculer-db" mixin registers the following actions:
		 *  - list
		 *  - find
		 *  - count
		 *  - create
		 *  - insert
		 *  - update
		 *  - remove
		 */

		// --- ADDITIONAL ACTIONS ---

		/**
		 * Get the greeting according to the part of day.
		 */
		currentGreeting: {
			rest: "GET /:hour",
			params: { hour: "string|min:3" },
			async handler(ctx) {
				const doc = await this.adapter.find({at: ctx.params.hour});
				const json = await this.transformDocuments(ctx, ctx.params, doc);
				return json;
			}
		},
	}
};
