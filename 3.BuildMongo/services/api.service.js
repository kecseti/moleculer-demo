const ApiGateway = require("moleculer-web");

module.exports = {
	name: "api",
	mixins: [ApiGateway],

	settings: {
		// Exposed port
		port: process.env.PORT || 3000,

		// Exposed IP
        ip: "0.0.0.0",

        routes: [
			{
				path: "/api",

				whitelist: [ "**" ],

				// The auto-alias feature allows you to declare your route alias directly in your services.  
				// The gateway will dynamically build the full routes from service schema.
				autoAliases: true,

				bodyParsers: {
					json: {
						strict: false,
						limit: "1MB"
					},
					urlencoded: {
						extended: true,
						limit: "1MB"
					}
				},

				// Enable/disable logging
				logging: true
			}
        ],
    }
}