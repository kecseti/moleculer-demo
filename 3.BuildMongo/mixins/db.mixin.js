const DbService	= require("moleculer-db");

module.exports = function(collection) {
	const schema = {
		mixins: [DbService]
	};

	if (process.env.MONGO_URI) {
		// Mongo adapter
		const MongoAdapter = require("moleculer-db-adapter-mongo");
		schema.adapter = new MongoAdapter(process.env.MONGO_URI);
		schema.collection = collection;
	} else {
		// NeDB memory adapter for testing
		schema.adapter = new DbService.MemoryAdapter();
	} 
	return schema;
};
