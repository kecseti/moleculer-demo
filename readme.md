# 1. Let's get familiar
### Name
```javascript
name:'greeter'
```
Every service has a name wich is a mandatory field.

---
### Version
```javascript
version:1
```
Using the version we can have multiple version of the same service and we van reffer to them by the version number. It can be either a number or a string.

---
### Events
```javascript
events:{
    'greetingOnLogin'(payload) {
        console.log(`Hello ${payload.name}!`);
    },
},
```
A service can subscribe to emited events. These events can be emited by the broker or by other services.
A sevice subscribes to an event by simply specifying the event name. Any event can have payload. 

---
### Actions
```javascript
actions: {
    hello() {
        return 'Hello World';
    },
},
```
The actions are the callable/public methods of the service.  
They are callable with broker.call.  
The action could be a function (as seen with the hello action) or an object with some properties(as seen in the howdy action).  
When an object definied we can specify the parameters and some basic validation for them.
In this case the parameter name can only be a string, any other case will result an error message.

The actions can have a parameter `context` or for short `ctx`. With the help of this parmeter we can reach the params passed to the action using `ctx.params`. Using the `ctx.call` we can call other action within actions or by using `ctx.call` we can emit diferent events. More about the `context` and its field here: https://moleculer.services/docs/0.12/context.html

---
### Methods
```javascript
methods: {
    generateGreetingMessage() {
       return 'Randomly generated greeting message';
    }
}
```
Methods are the private function of the service.These functuions are private and can not be called from another service or by the broker. You can call it from within the service, from event handlers, from actions or hooks.

---
### Lifecycle events
```javascript
created() {
    console.log('Fired when the service instance created.');
},

async started() {
    console.log('Fired when `broker.start()` called.');
}

async stopped() {
    console.log('Fired when `broker.stop()` called.');
}

```
There are some lifecycle service events that will be triggered by the broker.

---
### Mixins
```javascript
const ApiGwService = require("moleculer-web");

module.exports = {
    name: "api",
    mixins: [ApiGwService]
```
The Service constructor merges the mixins with the current schema. When a service uses mixins, all properties in the mixin will be “mixed” into the current service.

### Our firsrt service looks like this 
Note: we dont use mixins, yet.
```javascript
module.exports = {
    name: "greeter",

    events:{
        "greetingOnLogin"(payload) {
            console.log("Hello", payload.name);
        },
    },

    actions: {
        hello() {
            return 'Hello World';
        },
        howdy:{
            params: {
                name: "string ",
            },
            handler(ctx){
                return `Howdy ${ctx.params.name}?`;
            }
        }
    },

    methods: {
        generateGreetingMessage() {
            return 'Randomly generated greeting message';
        }
    },

    created() {
        console.log('Fired when the service instance created.');
    },
    
    async started() {
        console.log('Fired when `broker.start()` called.');
    },
    
    async stopped() {
        console.log('Fired when `broker.stop()` called.');
    }

};

```

---
## Run our first service
On a cmd simply type `npm run dev`.  
What that will do is start up a service broker, and scan the specified folder (in this case services) for available services (*.service.js) and register them as service.  
We specify here --hot for hot reload a service when it is changed and --repl for REPL mode, so we can reach our services from the cmd. 

First thing first, let see our nodes, type: `nodes -d`.  
This will show up one single node with two service in it.
What is realz cool about moleculer that in development, all services can be on a single node, as a monilith,
but when deploying these services, without any change to the service code, simply specify a transporter (NTAS) and
load one service per node as a microservice architecture.

For us to see the services type: `services -d`.  
This will show up two services, one being the one we defined, the greeter and the other one is a predefined service for every node, it contains usefull information about the node.

Lets see our defined action, type. `actions -d`.  
This will show a detailed view of all the available functions and their parameters. 

Smilarly the events defined can be also visualsized by typeing: `events -d`.

When the broker loaded and started our greeter service, we got the two message from the lifecycle methods.

To call our defined actions, as a broker would call them, we need the same functions.  
`call greeter.hello` will call the hello action.  
`call greeter.howdy '{"name": "Dave"}'` OR `call greeter.howdy --name Dave`.  
To pass the arguments we have to options in the command line, first is to pass them as a JSON object, like a broker would do it, the other one is to use command line arguments.
By calling it with anyting other than a string it should give back an error message like this:
```javascript
Data:  [
  {
    type: 'string',
    message: "The 'name' field must be a string.",
    field: 'name',
    actual: 9,
    nodeID: 'cs1335w-13596',
    action: 'greeter.howdy'
  }
]
``` 

To emmit an event, that our service is subscribed to use
`emit greetingOnLogin --name Dave`, the event will be called and the payload will be passed to that.

To stop th current process simply type `exit`. The last lifecycle method should be called, when the broker is stopped.

# 2. Build upp a webservice

Till now we only get familiar with the structure of the service and how it is interacting with other services or the broker. Lets build a simple webservice, so our actions can be reached by rest endpoints.
Run:  `npm install moleculer-web`  
To begin, we must introduce some new concepts.
We already mentioned mixins, they will be usefull here, also we must mention the settings field.
In the settings we can define various properties wich our service has.
```javascript
settings: {
    // Exposed port
    port: process.env.PORT || 3000,

    // Exposed IP
    ip: "0.0.0.0",

    routes: [
        {
            path: "/api",

            whitelist: [ "**" ],

            // The auto-alias feature allows you to declare your route alias directly in your services.  
            // The gateway will dynamically build the full routes from service schema.
            autoAliases: true,

            bodyParsers: {
                json: {
                    strict: false,
                    limit: "1MB"
                },
                urlencoded: {
                    extended: true,
                    limit: "1MB"
                }
            },

            // Enable/disable logging
            logging: true
        }
    ],
}
```
With the above setting we can set up a basic webservice, the only other thing is to use the proper mixin.  
First import the package `const ApiGateway = require("moleculer-web");` then within the service, specify the mixin:
`mixins: [ApiGateway],`.  
More of the setting on moleculer-web here: https://moleculer.services/docs/0.12/moleculer-web.html

Our first api gateway looks like this:
```javascript
const ApiGateway = require("moleculer-web");

module.exports = {
	name: "api",
	mixins: [ApiGateway],

	settings: {
		// Exposed port
		port: process.env.PORT || 3000,

		// Exposed IP
        ip: "0.0.0.0",

        routes: [
			{
				path: "/api",

				whitelist: [ "**" ],

				// The auto-alias feature allows you to declare your route alias directly in your services.  
				// The gateway will dynamically build the full routes from service schema.
				autoAliases: true,

				bodyParsers: {
					json: {
						strict: false,
						limit: "1MB"
					},
					urlencoded: {
						extended: true,
						limit: "1MB"
					}
				},

				// Enable/disable logging
				logging: true
			}
        ],
    }
}

```

---
### Mappings
```javascript
rest: {
	method: "GET",
	path: "/hello"
},
```
We can modify our greeter service and add REST mappings to it. By doing so, we expose our actions to the public.  
We can specify this rest field similarly to the params field, inside the action.
The modified actions with the resat mapping looks like this:
```javascript
actions: {
    hello:{
        rest: {
            method: "GET",
            path: "/hello"
        },
        handler () {
            return 'Hello World';
        }   
    },
    howdy:{
        rest: "/howdy",
        params: {
            name: "string ",
        },
        handler(ctx){
            return `Howdy ${ctx.params.name}?`;
        }
    }
},
```

---
### Hooks
```javascript
hooks:{
    before:{
        hello(){
            console.log('Hello before hook');
        }
    },
    after:{
        '*'(ctx, res){
            console.log('All action after hook');
            return res;
        }
    }
},
```
The hooking mechanism can be realy usefull. In a service, we can specify three diferent type of hooks:
`before`, `after` and `error`. The before hooks most common use cases are to sanitize data, to set default values to fields or to authorize. The after hooks most comonly used to check data goven back to the user and hide not wanted field(eg, password hash fields). The error hook is an error handler.  
It is possible to set hook to all action by using:
```javascript
'*'(){
}
```
Or to specific actions, using the actions name. 

### Let's run our service and test it in postman/insomnia/browser.
To call our first 'endpoint' use: `localhost:3000/api/greeter/hello`, this should give back a string with Hello World in it.
To call our second 'endpoint' use: `localhost:3000/api/greeter/howdy?name=Dave`, this should give back a string with Hello Dave in it. Notice that we specified the parameter in the header. If we miss that or we dont give a proper string moleculer will give back an error message.
Notice on the console the hook messages that we just printed out.


Besides the endpoints we just made, there is one more to list all endpoints available.  
Lets modify our response on the after hook and add the nodeID to it.
```javascript
after:{
    '*'(ctx, res){
        console.log('All action after hook');
        res +=` from ${this.broker.nodeID}`;
        return res;
    }
}
```

## Docker Compose
Rigth now we have only one node in wich runs the api gateway and the a greeter service.
We now want to separate them to nodes and make two greeter services.
We dont need to change the code at all. Just specify a docker-compose.yml file, based on wich moleculer can build up the system for us.  
We use our services, the api and two of the greeter service, also we use two more thing. 
Nats for communication between nodes and traefik to handle request.  
Run `npm run dc:up`. After that we should get a system that is built up from 5 containers, one for nats, one for traefik, one for our api gateway and two for our greeter services.  
By calling eiher endpoint multiple times we can see that the nodeID swithces between the two greeter service nodeIDs.

# 3. Build something usefull
Let's introduce a new part to our puzzle, the database. Moleculer offers a lot of DB connectors, for MongoDB, Postgress, MySQL, SQLite and MSSQL, for now we will use MongoDB. 

## Build it up!
---
As basic block wi will need almost the same pieces, an api gateway and a service, the only diference will be that we will create our own mixin, wich already has a mixin from `moleculer-db`. So basically we will get the code from `moleculer-db`, add our own to it  and mix it in to our service. 

## The API gateway
---
The api compared to the previous one will not change, we will use the same settings for it.

## The Mixin
---
We will create a new folder called `mixins`, and create a file in it called `db.mixin.js`. 
In This file we will create a function, which gets the collection name as parameter and uses the molecular-db as a mixin, to implement most of the basic methods (find, list-with pagination, count, create, insert, update, remove).  
The returned object is the schema, wich contains the mixins, the adatper, the collection and additinaly we can define here events and methods as well as lifcycle methods. 
For now we keep things on the bare minimum.
```javascript
const DbService	= require("moleculer-db");

module.exports = function(collection) {
	const schema = {
		mixins: [DbService]
	};

	if (process.env.MONGO_URI) {
		// Mongo adapter
		const MongoAdapter = require("moleculer-db-adapter-mongo");
		schema.adapter = new MongoAdapter(process.env.MONGO_URI);
		schema.collection = collection;
	} else {
		// NeDB memory adapter for testing
		schema.adapter = new DbService.MemoryAdapter();
	} 
	return schema;
};
```
We specify the mixin, then if there is a mongo URI then use mongo adapter with the given URI and the given collection name, 
else just use an in memory datastorage, for testing and developing pourposes mainly.

## The service
---
Lets stick with the greetings. In the database we store different greetings based on the part of the day.
Our collection will look something like this: 
```javascript
{   
    _id: 123,
    greeting: 'Good afternoon',
    at: '15:00'
}
```
In the service we will use our freshly created mixin, in the setting we will set wich fields to include on a response and set validation for create and insert actions.  
Then if the autamically added actions are not enough for our needs, we can specify our own. 
Let's create an action which will give back the correct greeting based on the part of the day.

```javascript
const DbMixin = require("../mixins/db.mixin");

module.exports = {
	name: "greetings",
	mixins: [DbMixin("greetings")],

	settings: {
		// Available fields in the responses
		fields: ["_id", "greeting",],

		// Validator for the `create` & `insert` actions.
		entityValidator: {
			greeting: "string|min:4",
			at: "string|min:3"
		}
    },
    
	actions: {
		/**
		 * The "moleculer-db" mixin registers the following actions:
		 *  - list
		 *  - find
		 *  - count
		 *  - create
		 *  - insert
		 *  - update
		 *  - remove
		 */

		currentGreeting: {
			rest: "GET /:hour",
			params: { hour: "string|min:3" },
			async handler(ctx) {
				const doc = await this.adapter.find({at: ctx.params.hour});
				const json = await this.transformDocuments(ctx, ctx.params, doc);
				return json;
			}
		},
	}
};
```
Try it out!
Run `npm run dev` then insert from postman/insomnia using POST
`http://localhost:3000/api/greeting`
```javascript
{
    "greeting":"Hello",
    "at":"00:00"
}
```
Let's assume that the already defined actions work, test our own with GET `http://localhost:3000/api/greeting/00:00`  
The just created greeting should come back, but only the ida and the greeting itself, not the at field.

## Kubernetes
---
For the last learning exercise we used docker compose. For now let's use something more fancy, kubernetes. The deployment is as easy as the previous one, only one .yaml file is made, most of its part is premade (nats, traefik, mongo,persistent volume for mongodb) we only should care about the services we just created and create a deployment for them.
That part should be as simple as this:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: greeter
spec:
  selector:
    matchLabels:
      app: greeter
  replicas: 2
  template:
    metadata:
      labels:
        app: greeter
    spec:
      containers:
      - name: greeter
        image: moleculer-demo
        envFrom:
        - configMapRef:
            name: common-env
        env:
          - name: SERVICES
            value: greeter
```
Using kubectl apply this deployment: `kubectl apply -f k8s.yaml`.  